import { useState, useEffect } from "react";

const BasicForm = () =>{
    const [firstName, setFirstName] = useState(localStorage.getItem("firstName")||"");
    const [lastName, setLastName] = useState(localStorage.getItem("lastName")||"");

    // Hàm xử lý sự kiện khi ô văn bản 1 thay đổi 
    const onTextbox1Change = (event) =>{
        setFirstName(event.target.value);
    }

    // Hàm xử lý sự kiện khi ô văn bản 2 thay đổi 
    const onTextbox2Change = (event) =>{
        setLastName (event.target.value);
    }

    // Sử dụng useEffect để cập nhật vào localStorage
    useEffect (()=>{
        localStorage.setItem("firstName", firstName);
        localStorage.setItem("lastName", lastName);
    }, [firstName,lastName]);
    return (
        <div style = {{marginLeft: "50px"}}>
            <p>
                <input type = "text" onChange = {onTextbox1Change} value = {firstName}/>
            </p>
            <p>
                <input type = "text" onChange = {onTextbox2Change} value = {lastName}/>
            </p>
            <p>
                {firstName} {lastName}
            </p>
        </div>
    )
}

export default BasicForm;